# EP1 - OO (UnB - Gama)

Daniel Marques Rangel - 15/0008228

Instruções: 

 1- Compilar o programa pelo Makefile usando o g++ e executá-lo, usando os seguintes comandos no terminal:
     $ make
     $ make run
     ou $ make && make run
 
 2- Digite o número correspondente a opção de filtro desejada no menu principal e aperte Enter;
     ex: 1 | Enter
 
 3- Digite o diretório da imagem que você deseja colocar o filtro (não esquecer da barra final "/");
     ex: /home/Daniel/Documentos/OO/EP1/doc/
 
 4- Digite o nome da imagem (sem o ".ppm");
     ex: xmas
 
 OBS: no filtro Média será apresentando outro menu para escolher o tipo de máscara desejada, isso após ter digitado diretório e nome da imagem;
     ex: 3 | Enter
 
 5- Irá ser apresentado os dados da imagem;
 
 6- Digite o diretório da nova imagem a ser salva;
     ex: /home/Daniel/Documentos/OO/EP1/doc/
 
 7- Após a imagem ser salva, o programa voltará para o menu, caso queira inserir outro filtro escolha uma nova opção, caso queira sair digite "5";
     ex: 5 | Enter
