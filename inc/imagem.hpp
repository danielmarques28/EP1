#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class Imagem{
  string formato;
  int largura;
  int altura;
  int maximo;
  unsigned char **red, **green, **blue;
public:
  Imagem();
	~Imagem();
	string getFormato();
	void setFormato(string formato);
	int getLargura();
	void setLargura(int largura);
	int getAltura();
	void setAltura(int largura);
	int getMaximo();
	void setMaximo(int maximo);
	unsigned char** getRed();
  void setRed(unsigned char **red);
  unsigned char** getGreen();
  void setGreen(unsigned char **green);
  unsigned char** getBlue();
  void setBlue(unsigned char **blue);
	char lerImagem(char erro_arquivo);
  void gravarImagem(unsigned char **red,unsigned char **green,unsigned char **blue, string nomeArquivo);

};

#endif
