#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP

#include "filtro.hpp"

class Polarizado : public Filtro{
  unsigned char **red_polarizado, **green_polarizado, **blue_polarizado;
public:
    Polarizado();
    ~Polarizado();
    unsigned char** getRed_polarizado();
    void setRed_polarizado(unsigned char **red_polarizado);
    unsigned char** getGreen_polarizado();
    void setGreen_polarizado(unsigned char **green_polarizado);
    unsigned char** getBlue_polarizado();
    void setBlue_polarizado(unsigned char **blue_polarizado);

    void insercaoFiltro();
};

#endif
