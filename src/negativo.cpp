#include "negativo.hpp"

Negativo::Negativo(){

}
Negativo::~Negativo(){

}
unsigned char** Negativo::getRed_negativo(){
  return red_negativo;
}
void Negativo::setRed_negativo(unsigned char **red_negativo){
  this->red_negativo = red_negativo;
}
unsigned char** Negativo::getGreen_negativo(){
  return green_negativo;
}
void Negativo::setGreen_negativo(unsigned char **green_negativo){
  this->green_negativo = green_negativo;
}
unsigned char** Negativo::getBlue_negativo(){
  return blue_negativo;
}
void Negativo::setBlue_negativo(unsigned char **blue_negativo){
  this->blue_negativo = blue_negativo;
}

void Negativo::insercaoFiltro(){
  unsigned char max_cor;
  unsigned char **red, **green, **blue;

	red = getRed();
	green = getGreen();
	blue = getBlue();
  max_cor = getMaximo();

  unsigned char **aux_red = new unsigned char *[getLargura()];
  unsigned char **aux_green = new unsigned char *[getLargura()];
  unsigned char **aux_blue = new unsigned char *[getLargura()];

  for(int aux=0;aux < getAltura();aux++){
    aux_red[aux] = new unsigned char [getLargura()];
    aux_green[aux] = new unsigned char [getLargura()];
    aux_blue[aux] = new unsigned char [getLargura()];
  }

  for(int linha = 0;linha < getAltura();linha++){
    for(int coluna = 0;coluna < getLargura();coluna++){
      aux_red[linha][coluna]= red[linha][coluna];
  		aux_green[linha][coluna] = green[linha][coluna];
  		aux_blue[linha][coluna] = blue[linha][coluna];
    }
  }

  for(int linha = 0;linha < getAltura();linha++){
    for(int coluna = 0;coluna < getLargura();coluna++){
      aux_red[linha][coluna]= (max_cor - aux_red[linha][coluna]);
  		aux_green[linha][coluna] = (max_cor - aux_green[linha][coluna]);
  		aux_blue[linha][coluna] = (max_cor - aux_blue[linha][coluna]);
    }
  }

  setRed_negativo(aux_red);
  setGreen_negativo(aux_green);
  setBlue_negativo(aux_blue);

  gravarImagem(getRed_negativo(), getGreen_negativo(), getBlue_negativo(), "filtroNegativo");

}
