#include "negativo.hpp"
#include "polarizado.hpp"
#include "pretoBranco.hpp"
#include "media.hpp"

#define NUM_LINHAS 50

char validaMenu(char *menu){
  while(*menu < '1' || *menu > '5') {
    cout << "Digite uma opcao valida.\n\nOpcao: ";
    cin >> menu;
  }
  return *menu;
}

int main() {
  char menu, erro_arquivo;
  cout << string(NUM_LINHAS, '\n');

    Negativo *ppm_negativo;
    Polarizado *ppm_polarizado;
    PretoBranco *ppm_preto_branco;
    Media *ppm_media;

      do {
        cout << "MENU de Filtro:\n1- Negativo\n2- Polarizado\n3- Preto e Branco\n4- Media\n5- SAIR\n\n\nOpcao: ";
        cin >> menu;
        menu = validaMenu(&menu);
        erro_arquivo = '0';
        switch (menu) {
          case '1':{
            ppm_negativo = new Negativo();
            erro_arquivo = ppm_negativo->lerImagem(erro_arquivo);
            if(erro_arquivo != '1')
              ppm_negativo->insercaoFiltro();
            delete(ppm_negativo);
            break;
          }
          case '2':{
            ppm_polarizado = new Polarizado();
            erro_arquivo = ppm_polarizado->lerImagem(erro_arquivo);
            if(erro_arquivo != '1')
              ppm_polarizado->insercaoFiltro();
            delete(ppm_polarizado);
            break;
          }
          case '3':{
            ppm_preto_branco = new PretoBranco();
            erro_arquivo = ppm_preto_branco->lerImagem(erro_arquivo);
            if(erro_arquivo != '1')
              ppm_preto_branco->insercaoFiltro();
            delete(ppm_preto_branco);
            break;
          }
          case '4':{
            ppm_media = new Media();
            erro_arquivo = ppm_media->lerImagem(erro_arquivo);
            if(erro_arquivo != '1')
              ppm_media->insercaoFiltro();
            delete(ppm_media);
          }
        }
    }while(menu != '5');

  return 0;
}
