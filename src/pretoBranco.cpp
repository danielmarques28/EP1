#include "pretoBranco.hpp"

PretoBranco::PretoBranco(){

}
PretoBranco::~PretoBranco(){

 }
unsigned char** PretoBranco::getRed_preto_branco(){
  return red_preto_branco;
}
void PretoBranco::setRed_preto_branco(unsigned char **red_preto_branco){
  this->red_preto_branco = red_preto_branco;
}
unsigned char** PretoBranco::getGreen_preto_branco(){
  return green_preto_branco;
}
void PretoBranco::setGreen_preto_branco(unsigned char **green_preto_branco){
  this->green_preto_branco = green_preto_branco;
}
unsigned char** PretoBranco::getBlue_preto_branco(){
  return blue_preto_branco;
}
void PretoBranco::setBlue_preto_branco(unsigned char **blue_preto_branco){
  this->blue_preto_branco = blue_preto_branco;
}

void PretoBranco::insercaoFiltro(){
  unsigned char escala_cinza;
  unsigned char **red, **green, **blue;

	red = getRed();
	green = getGreen();
	blue = getBlue();

  unsigned char **aux_red = new unsigned char *[getLargura()];
  unsigned char **aux_green = new unsigned char *[getLargura()];
  unsigned char **aux_blue = new unsigned char *[getLargura()];

  for(int aux=0;aux < getAltura();aux++){
    aux_red[aux] = new unsigned char [getLargura()];
    aux_green[aux] = new unsigned char [getLargura()];
    aux_blue[aux] = new unsigned char [getLargura()];
  }

  for(int linha = 0;linha < getAltura();linha++){
    for(int coluna = 0;coluna < getLargura();coluna++){
      aux_red[linha][coluna]= red[linha][coluna];
  		aux_green[linha][coluna] = green[linha][coluna];
  		aux_blue[linha][coluna] = blue[linha][coluna];
    }
  }

  for(int linha = 0;linha < getAltura();linha++){
    for(int coluna = 0;coluna < getLargura();coluna++){
      escala_cinza = ((0.299 *aux_red[linha][coluna]) + (0.587 *aux_green[linha][coluna]) + (0.144 *aux_blue[linha][coluna]));

  		aux_red[linha][coluna] = escala_cinza;
  		aux_green[linha][coluna] = escala_cinza;
  		aux_blue[linha][coluna] = escala_cinza;
    }
	}

  setRed_preto_branco(aux_red);
  setGreen_preto_branco(aux_green);
  setBlue_preto_branco(aux_blue);

  gravarImagem(getRed_preto_branco(), getGreen_preto_branco(), getBlue_preto_branco(), "filtroPretoBranco");

}
